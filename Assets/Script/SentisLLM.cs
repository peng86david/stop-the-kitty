using UnityEngine;
using Unity.Sentis;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System;

public class SentisLLM : MonoBehaviour
{
    [SerializeField] string prompt = "";
    [SerializeField] ModelAsset modelAsset;
    [SerializeField] int vocabularySize = 50257; // Typical vocabulary size for GPT-NeoX
    private const int SequenceLength = 128;
    private const int EmbeddingSize = 32; // Adjust this according to your model's output embedding size
    private Model runtimeModel;
    private IWorker worker;

    void Start()
    {
        runtimeModel = ModelLoader.Load(modelAsset);
        worker = WorkerFactory.CreateWorker(BackendType.CPU, runtimeModel);
        print(RunInference(prompt));
    }

    public string RunInference(string inputText)
    {
        (TensorInt inputIds, TensorFloat attentionMask) = Tokenize(inputText);

        var inputs = new Dictionary<string, Tensor>
        {
            { "input_ids", inputIds },
            { "attention_mask", attentionMask }
        };

        worker.Execute(inputs);

        // Get the output tensor
        TensorFloat outputTensor = worker.PeekOutput() as TensorFloat;

        if (outputTensor == null)
        {
            Debug.LogError("Failed to retrieve output tensor");
            return string.Empty;
        }

        string outputText = Detokenize(outputTensor);

        inputIds.Dispose();
        attentionMask.Dispose();

        return outputText;
    }

    private (TensorInt, TensorFloat) Tokenize(string text)
    {
        // Replace this with a proper tokenizer that matches the one used during training of the model
        List<int> tokens = text.Select(c => (int)c % vocabularySize).ToList();

        int actualLength = Mathf.Min(tokens.Count, SequenceLength);
        tokens = tokens.Take(actualLength).ToList();
        tokens.AddRange(Enumerable.Repeat(0, SequenceLength - actualLength));

        var inputShape = new TensorShape(1, SequenceLength);
        var inputIds = new TensorInt(inputShape, tokens.ToArray());

        var attentionMask = new float[SequenceLength];
        for (int i = 0; i < actualLength; i++)
            attentionMask[i] = 1f;
        var attentionMaskTensor = new TensorFloat(inputShape, attentionMask);

        return (inputIds, attentionMaskTensor);
    }

    private string Detokenize(TensorFloat tensor)
{
    if (tensor == null)
    {
        Debug.LogError("Output tensor is null");
        return string.Empty;
    }

    float[] embeddings = tensor.ToReadOnlyArray();

    // Initialize StringBuilder for the result
    StringBuilder result = new StringBuilder();

    // Check tensor shape and process accordingly
    if (tensor.shape.rank == 3 && tensor.shape[0] == 1 && tensor.shape[1] == SequenceLength && tensor.shape[2] == EmbeddingSize)
    {
        for (int i = 0; i < SequenceLength; i++)
        {
            // Project embeddings to logits over the vocabulary size
            float[] logits = new float[vocabularySize];
            for (int j = 0; j < EmbeddingSize; j++)
            {
                for (int k = 0; k < vocabularySize; k++)
                {
                    logits[k] += embeddings[i * EmbeddingSize + j] * UnityEngine.Random.Range(-0.1f, 0.1f); // Random projection
                }
            }

            // Use Argmax to get the most likely token ID
            int maxIdx = Array.IndexOf(logits, logits.Max());

            // Convert tokenId back to character (adjust this based on your tokenizer)
            char c = (char)maxIdx;

            // Append the character to result
            result.Append(c);
        }
    }
    else
    {
        Debug.LogError($"Unexpected tensor shape: {string.Join(", ", tensor.shape)}");
        return string.Empty;
    }

    return result.ToString().TrimEnd('\0'); // Trim any trailing null characters
}


    void OnDestroy()
    {
        worker?.Dispose();
    }
}
