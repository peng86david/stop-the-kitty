using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Stocks : MonoBehaviour
{
    public float price = 1.0f;
    float period = 0.0f, shortInfluencePeriod = 0.0f;

    public List<float> stockPrices = new List<float>();
    public WindowGraph graph;
    public TMP_Text stockPrice;
    public float priceJump = 0.0f, shortInfluence = 0.0f;
    public SocialMedia socialMedia;
    List<Post> postRef = new List<Post>();
    public bool paused = false;
    // Start is called before the first frame update
    void Start()
    {
        socialMedia = (SocialMedia)FindObjectOfType(typeof(SocialMedia));
    }

    // Update is called once per frame
    void Update()
    {
        if (!paused){
            PriceCalc();
        }
    }

    void PriceCalc(){
    if (period > 4.0f) //every 4 seconds = 1 minute in real world. stock market opens 390 minutes every day
        {
            postRef = socialMedia.GetPosts();
            float logShort = 0;
            if(postRef.Count >0)
                priceJump = (postRef[postRef.Count -1].sentiment)/2;
            stockPrices.Add(price);
            float probabilty = 0;
            if(priceJump < 0){
                probabilty = UnityEngine.Random.Range(priceJump, -priceJump/2);
            }
            else{
                probabilty = UnityEngine.Random.Range(-priceJump/2, priceJump);
            }

            if(shortInfluencePeriod > 3.0f){
                shortInfluence = 0f;
                shortInfluencePeriod = 0;
            }
            if(shortInfluence > 0) {
                logShort = - Mathf.Log(Mathf.Abs(price * shortInfluence + 1), 1/20);
            }
            else{
                logShort = Mathf.Log(Mathf.Abs(price * shortInfluence + 1), 1/20);
            }

            price += 10 * probabilty * probabilty * probabilty + logShort;

            price = (float)Math.Round(price, 2);
            stockPrice.text = price.ToString("C");
            graph.ShowGraph(stockPrices);
            period = 0;
        }
        period += UnityEngine.Time.deltaTime;
        shortInfluencePeriod += UnityEngine.Time.deltaTime;
        
}
}

